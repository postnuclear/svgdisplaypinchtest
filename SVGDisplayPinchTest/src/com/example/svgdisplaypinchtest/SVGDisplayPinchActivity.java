package com.example.svgdisplaypinchtest;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import android.support.v7.app.ActionBarActivity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class SVGDisplayPinchActivity extends ActionBarActivity 
{
	ImageView image;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_svgdisplay_pinch);
		
		image = (ImageView) findViewById(R.id.imageView1);

		image.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		
		SVG svg = SVGParser.getSVGFromResource(getResources(), R.raw.biuro);
        // Picture picture = svg.getPicture();
        Drawable drawable = svg.createPictureDrawable();		
		// Get a drawable from the parsed SVG and set it as the drawable for the ImageView
        image.setImageDrawable(drawable); 	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.svgdisplay_pinch, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}